FROM --platform=$TARGETPLATFORM alpine

ARG TARGETPLATFORM

COPY target/${TARGETPLATFORM}/compute /compute

ENTRYPOINT ["/compute"]
