struct Vote {
    agree: bool,
    pet: measurements::Length,
    diff: measurements::Length,
}

pub struct Quorum {
    votes: Vec<Vote>,
    rain: measurements::Length,
}

impl Quorum {
    pub fn new(rain: measurements::Length) -> Quorum {
        Quorum {
            votes: Vec::new(),
            rain: rain,
        }
    }

    pub fn with_vote(&mut self, pet: measurements::Length) -> &mut Self {
        self.votes.push(Vote {
            agree: self.rain < pet,
            pet: pet,
            diff: self.rain - pet,
        });
        self
    }

    pub fn need_water(&self) -> (bool, measurements::Length, measurements::Length) {
        (
            self.votes
                .iter()
                .map(|vote| match vote.agree {
                    true => 1,
                    false => 0,
                })
                .reduce(|acc, v| acc + v)
                .unwrap()
                > (self.votes.len() / 2),
            self.votes
                .iter()
                .map(|vote| vote.diff)
                .reduce(|acc, diff| acc + diff)
                .unwrap()
                / (self.votes.len() as f64),
            self.votes
                .iter()
                .map(|vote| vote.pet)
                .reduce(|acc, pet| acc + pet)
                .unwrap()
                / (self.votes.len() as f64),
        )
    }
}
#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_quorum() {
        assert!(
            Quorum::new(measurements::Length::from_millimetres(0.))
                .with_vote(measurements::Length::from_millimetres(2.))
                .need_water()
                .0
        );
        assert!(
            !Quorum::new(measurements::Length::from_millimetres(2.))
                .with_vote(measurements::Length::from_millimetres(2.))
                .need_water()
                .0
        );
        assert!(
            Quorum::new(measurements::Length::from_millimetres(1.5))
                .with_vote(measurements::Length::from_millimetres(1.))
                .with_vote(measurements::Length::from_millimetres(2.))
                .with_vote(measurements::Length::from_millimetres(2.))
                .need_water()
                .0
        );
        assert!(
            !Quorum::new(measurements::Length::from_millimetres(1.5))
                .with_vote(measurements::Length::from_millimetres(1.))
                .with_vote(measurements::Length::from_millimetres(1.))
                .with_vote(measurements::Length::from_millimetres(2.))
                .need_water()
                .0
        );
    }
}
