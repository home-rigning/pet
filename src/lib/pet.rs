// https://en.wikipedia.org/wiki/Tetens_equation
fn tetens(temp: measurements::Temperature) -> measurements::Pressure {
    measurements::Pressure::from_kilopascals(
        0.6108 * libm::exp(17.27 * temp.as_celsius() / (temp.as_celsius() + 237.3)),
    )
}

pub fn dalton(
    temp_min: measurements::Temperature,
    temp_max: measurements::Temperature,
    rh_min: measurements::Humidity,
    rh_max: measurements::Humidity,
    wind: measurements::Speed,
) -> measurements::Length {
    let tetens_temp_min = tetens(temp_min);
    let tetens_temp_max = tetens(temp_max);

    let e_sat = (tetens_temp_min + tetens_temp_max) / 2.;
    let e_atm = (tetens_temp_min * rh_max.as_ratio() + tetens_temp_max * rh_min.as_ratio()) / 2.;

    measurements::Length::from_millimeters(
        (3.648 + 0.7223 * wind.as_meters_per_second())
            * (e_sat.as_kilopascals() - e_atm.as_kilopascals()),
    )
}

pub fn romanenko(
    temp_min: measurements::Temperature,
    temp_max: measurements::Temperature,
    rh_min: measurements::Humidity,
    rh_max: measurements::Humidity,
) -> measurements::Length {
    let temp_avg = (temp_min.as_celsius() + temp_max.as_celsius()) / 2.;
    let rh_avg = (rh_min.as_percent() + rh_max.as_percent()) / 2.;

    measurements::Length::from_millimeters(
        0.00006 * libm::pow(25. + temp_avg, 2.) * (100. - rh_avg),
    )
}

pub fn penman(
    temp_min: measurements::Temperature,
    temp_max: measurements::Temperature,
    rh_min: measurements::Humidity,
    rh_max: measurements::Humidity,
    wind: measurements::Speed,
) -> measurements::Length {
    let tetens_temp_min = tetens(temp_min);
    let tetens_temp_max = tetens(temp_max);

    let e_sat = (tetens_temp_min + tetens_temp_max) / 2.;
    let e_atm = (tetens_temp_min * rh_max.as_ratio() + tetens_temp_max * rh_min.as_ratio()) / 2.;

    measurements::Length::from_millimeters(
        (2.625 + 0.000479 / wind.as_meters_per_second())
            * (e_sat.as_kilopascals() - e_atm.as_kilopascals()),
    )
}

#[cfg(test)]
mod tests {
    use crate::pet::*;

    #[test_log::test]
    fn test_dalton() {
        measurements::test_utils::assert_almost_eq(
            dalton(
                measurements::Temperature::from_celsius(14.0),
                measurements::Temperature::from_celsius(21.0),
                measurements::Humidity::from_ratio(0.659),
                measurements::Humidity::from_ratio(0.975),
                measurements::Speed::from_meters_per_second(5.19),
            )
            .as_millimeters(),
            3.28427,
        );
    }

    #[test_log::test]
    fn test_romanenko() {
        measurements::test_utils::assert_almost_eq(
            romanenko(
                measurements::Temperature::from_celsius(14.0),
                measurements::Temperature::from_celsius(21.0),
                measurements::Humidity::from_ratio(0.659),
                measurements::Humidity::from_ratio(0.975),
            )
            .as_millimeters(),
            1.98326,
        );
    }

    #[test_log::test]
    fn test_penman() {
        measurements::test_utils::assert_almost_eq(
            penman(
                measurements::Temperature::from_celsius(14.0),
                measurements::Temperature::from_celsius(21.0),
                measurements::Humidity::from_ratio(0.659),
                measurements::Humidity::from_ratio(0.975),
                measurements::Speed::from_meters_per_second(5.19),
            )
            .as_millimeters(),
            1.16558,
        );
    }
}
