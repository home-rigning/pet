#[macro_use]
extern crate log;
extern crate compute;

use clap::Parser;
use influxdb::InfluxDbWriteable;

fn sum_rain(rains: (measurements::Length, measurements::Length)) -> measurements::Length {
    rains.0 + rains.1
}

#[derive(clap::Subcommand, Debug, Clone)]
enum Settings {
    #[clap(visible_alias = "influxdb")]
    InfluxDB {
        #[arg(long)]
        url: String,
    },
    #[clap(visible_alias = "influxdb2")]
    InfluxDB2 {
        #[arg(long)]
        url: String,

        #[arg(long)]
        org: String,

        #[arg(long)]
        token: String,
    },
}

#[derive(clap::Parser, Debug, Clone)]
#[clap(author, version, about)]
struct Arguments {
    #[command(subcommand)]
    settings: Settings,

    #[arg(long)]
    dry_run: bool,

    #[arg(default_value_t = 1, long)]
    /// Number of days
    days: u64,

    /// city code
    city: rigning::model::City,
}

async fn make_hub(
    settings: &Settings,
    city: &rigning::model::City,
) -> Box<dyn rigning::client::Hub> {
    match settings {
        Settings::InfluxDB2 { url, org, token } => Box::new(
            rigning::store::InfluxDB2::new(
                city.clone(),
                rigning::store::InfluxDB2Settings {
                    url: url.to_string(),
                    org: org.to_string(),
                    token: token.to_string(),
                },
            )
            .await,
        ),
        Settings::InfluxDB { url } => Box::new(
            rigning::store::InfluxDB::new(
                city.clone(),
                rigning::store::InfluxDBSettings {
                    url: url.to_string(),
                },
            )
            .await,
        ),
    }
}

#[tokio::main]
async fn main() {
    env_logger::init();

    let args = Arguments::parse();
    let hub = make_hub(&args.settings, &args.city).await;

    let r_sum = sum_rain(hub.rain("sum", args.days).await.unwrap());
    info!("sum rain: {}", r_sum);
    let h_max = hub.humidity("max", args.days).await.unwrap();
    info!("max humidity: {}", h_max);
    let h_min = hub.humidity("min", args.days).await.unwrap();
    info!("min humidity: {}", h_min);
    let t_max = hub.temp("max", args.days).await.unwrap();
    info!("max temperture: {}", t_max.as_celsius());
    let t_min = hub.temp("min", args.days).await.unwrap();
    info!("min temperture: {}", t_min.as_celsius());
    let w_mean = hub.wind("mean", args.days).await.unwrap();
    info!("mean wind: {}", w_mean);

    let romanenko = compute::pet::romanenko(t_min, t_max, h_min, h_max);
    let dalton = compute::pet::dalton(t_min, t_max, h_min, h_max, w_mean);
    let penman = compute::pet::penman(t_min, t_max, h_min, h_max, w_mean);

    let (need_water, diff, pet) = compute::utils::Quorum::new(r_sum)
        .with_vote(romanenko)
        .with_vote(dalton)
        .with_vote(penman)
        .need_water();

    info!(
        "Need to water: {}, rain: {}mm, diff {}mm (avg PET: {}mm)",
        need_water,
        r_sum.as_millimeters(),
        diff.as_millimeters(),
        pet.as_millimeters(),
    );

    if args.dry_run {
        return;
    }

    match args.settings {
        Settings::InfluxDB2 { url, org, token } => {
            let client = influxdb2::Client::new(url, org, token);
            let write_result = client
                .write(
                    "rigning",
                    futures::stream::iter(vec![influxdb2::models::DataPoint::builder("pet")
                        .tag("city", args.city.to_string())
                        .field("romanenko", romanenko.as_millimeters())
                        .field("dalton", dalton.as_millimeters())
                        .field("penman", penman.as_millimeters())
                        .field("rain", r_sum.as_millimeters())
                        .build()
                        .unwrap()]),
                )
                .await;
            assert!(write_result.is_ok(), "Write result was not okay");
        }
        Settings::InfluxDB { url } => {
            let client = influxdb::Client::new(url, "rigning");
            let write_result = client
                .query(vec![influxdb::Timestamp::from(chrono::Utc::now())
                    .into_query("pet")
                    .add_tag("city", args.city.to_string())
                    .add_field("romanenko", romanenko.as_millimeters())
                    .add_field("dalton", dalton.as_millimeters())
                    .add_field("penman", penman.as_millimeters())
                    .add_field("rain", r_sum.as_millimeters())])
                .await;
            assert!(write_result.is_ok(), "Write result was not okay");
        }
    }
}
